package database

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var db *mongo.Database

// Init launches mongodb connection
func Init() error {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb+srv://root:PLsN3wbhwfrRZ6aB@cluster0-groih.gcp.mongodb.net/test?retryWrites=true&w=majority"))
	if err != nil {
		return err
	}

	// Create connect
	err = client.Connect(context.TODO())
	if err != nil {
		return err
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return err
	}

	db = client.Database("hackevents")
	return nil
}

// Get returns database
func Get() *mongo.Database {
	return db
}

// C returns database collection
func C(name string) *mongo.Collection {
	return db.Collection(name)
}
