package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io"
	"io/ioutil"
	"math/rand"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
	"time"

	"go.mongodb.org/mongo-driver/bson"

	"github.com/gin-gonic/gin"
	"gitlab.com/soulmates/backend/database"
	"gitlab.com/soulmates/backend/models"
)

// Events ..
type Profile struct {
}

type ProfileResponse struct {
	Id       string
	Name     string
	Lastname string
	Image    string
}

type GetProfileResponse struct {
	Status  int
	Message string
	Data    models.Profile
}

type LikeProfileResponse struct {
	Status  int
	Message string
	Data    string
}

type DislikeProfileResponse struct {
	Status  int
	Message string
	Data    string
}

type SendStoryRequest struct {
	UserId string
	URL    string
}

type SendStoryResponse struct {
	Status  int
	Message string
	Data    string
}

func (h *Profile) Get(c *gin.Context) {
	prof := models.Profile{}
	cursor, err := database.C("profiles").Aggregate(c,

		bson.A{
			bson.M{"$match": bson.M{"firstname": bson.M{"$ne": "DELETED"}}},
			// bson.M{"$match": bson.M{"photo": bson.M{"$ne": "https://vk.com/images/camera_400.png?ava=1"}}},
			bson.M{"$match": bson.M{"photo": bson.M{"$ne": ""}}},
			bson.M{"$sample": bson.M{"size": 10}},
		},
	)
	if err != nil {
		fmt.Println(err)
	}

	for cursor.Next(c) {
		if err := cursor.Decode(&prof); err != nil {
			fmt.Println(err)
		}

		if len(prof.Groups) > 3 {
			break
		}
	}

	prof.Match = 30 + rand.Intn(70)

	// err = database.C("profiles").FindOne(c, bson.M{}).Decode(&prof)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	resp := GetProfileResponse{
		Status:  http.StatusOK,
		Message: "",
		Data:    prof,
	}

	c.JSON(http.StatusOK, &resp)
	return
}

func (h *Profile) Like(c *gin.Context) {
	resp := LikeProfileResponse{
		Status:  http.StatusOK,
		Message: "",
		Data:    c.Param("likedId"),
	}

	c.JSON(http.StatusOK, &resp)
	return
}

func (h *Profile) Dislike(c *gin.Context) {
	resp := LikeProfileResponse{
		Status:  http.StatusOK,
		Message: "",
		Data:    c.Param("dislikedId"),
	}

	c.JSON(http.StatusOK, &resp)
	return
}

func (h *Profile) SendStory(c *gin.Context) {
	r := SendStoryRequest{}
	err := c.Bind(&r)
	if err != nil {
		fmt.Println(err)
	}

	filename := makeStoryImage()

	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)
	fileWriter, err := bodyWriter.CreateFormFile("file", filename)
	if err != nil {
		fmt.Println(err)
	}

	fh, err := os.Open(filename)
	if err != nil {
		fmt.Println(err)
	}
	defer fh.Close()

	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		fmt.Println(err)
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	resp, err := http.Post(r.URL, contentType, bodyBuf)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(body))
}

func makeStoryImage() string {
	width := 848
	height := 1472

	upLeft := image.Point{0, 0}
	lowRight := image.Point{width, height}

	img := image.NewRGBA(image.Rectangle{upLeft, lowRight})

	a, b, c := uint8(rand.Intn(256)), uint8(rand.Intn(256)), uint8(rand.Intn(256))

	// Colors are defined by Red, Green, Blue, Alpha uint8 values.
	cyan := color.RGBA{a, b, c, 0xff}

	// Set color for each pixel.
	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			switch {
			case x < width && y < height: // upper left quadrant
				img.Set(x, y, cyan)
			case x >= width && y >= height: // lower right quadrant
				img.Set(x, y, color.White)
			default:
				// Use zero value.
			}
		}
	}

	// Encode as PNG.
	filename := strconv.Itoa(int(a)) + strconv.Itoa(int(b)) + strconv.Itoa(int(c)) + ".png"
	f, _ := os.Create(filename)
	png.Encode(f, img)

	return filename
}

type ParserProfileResponse struct {
	Response []struct {
		Id        int
		Firstname string `json:"first_name"`
		Lastname  string `json:"last_name"`
		Bdate     string
		Photo     string `json:"photo_400_orig"`
		Sex       int    // 1 - female, 2 - male
		City      struct {
			Title string
		}
	} `json:"response"`
}

type ParserGroupsResponse struct {
	Response struct {
		Count int
		Items []models.ProfileGroup
	}
}

func (h *Profile) Parser(c *gin.Context) {
	for idd := 230464756; idd <= 239465756; idd++ {
		id := strconv.Itoa(idd)
		fmt.Println(id)
		parseProfile := "https://api.vk.com/method/users.get?user_id=" + id + "&fields=photo_400_orig,city,bdate&v=5.52&access_token=0271afcdbb3d3e8b60aeee5083ddf8d07793ace84acd6d59bb5202fb20b338b6ca5704d628e0b8a00f139"

		resp, err := http.Get(parseProfile)
		if err != nil {
			fmt.Println(err)
		}
		defer resp.Body.Close()
		pr := ParserProfileResponse{}

		err = json.NewDecoder(resp.Body).Decode(&pr)
		if err != nil {
			fmt.Println(err, "err")
		}

		if len(pr.Response) == 0 {
			continue
		}

		fmt.Println(pr)

		parseGroups := "https://api.vk.com/method/groups.get?extended=1&fields=activity&user_id=" + id + "&v=5.52&access_token=0271afcdbb3d3e8b60aeee5083ddf8d07793ace84acd6d59bb5202fb20b338b6ca5704d628e0b8a00f139"

		resp, err = http.Get(parseGroups)
		if err != nil {
			fmt.Println(err)
		}
		defer resp.Body.Close()
		gr := ParserGroupsResponse{}

		err = json.NewDecoder(resp.Body).Decode(&gr)
		if err != nil {
			fmt.Println(err, "err")
		}

		_, err = database.C("profiles").InsertOne(c, models.Profile{
			Vkid:      id,
			Firstname: pr.Response[0].Firstname,
			Lastname:  pr.Response[0].Lastname,
			City:      pr.Response[0].City.Title,
			Age:       pr.Response[0].Bdate,
			Photo:     pr.Response[0].Photo,
			Sex:       pr.Response[0].Sex,
			Groups:    gr.Response.Items,
		})
		if err != nil {
			fmt.Println(err, "inserting database")
		}

		fmt.Println(gr)

		time.Sleep(time.Millisecond * 500)
	}
	c.JSON(http.StatusOK, nil)
	return
}
