package main

import (
	"os"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/soulmates/backend/database"
	"gitlab.com/soulmates/backend/handlers"
)

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	err := database.Init()
	if err != nil {
		log.Print(err)
	}

	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "GET", "POST"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	v1 := r.Group("/api/v1")

	profiles := handlers.Profile{}
	v1.GET("/profile/", profiles.Get)
	v1.GET("/profile/suggest/:userId", profiles.Get)
	v1.GET("/profile/like/:userId/:likedId", profiles.Like)
	v1.GET("/profile/dislike/:userId/:likedId", profiles.Dislike)

	v1.POST("/stories/send", profiles.SendStory)

	v1.GET("/parser", profiles.Parser)

	log.Info().Msg("server started")
	err = r.Run(":9000")

	if err != nil {
		log.Fatal().Err(err).Msg("server start failed")
	}
}
