package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Profile struct {
	ID   primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Vkid string

	Color   string
	Visited bool
	Match   int

	Firstname string
	Lastname  string
	City      string
	Age       string
	Photo     string

	Sex    int
	Groups []ProfileGroup
}

type ProfileGroup struct {
	Id       int
	Name     string
	Activity string
}
