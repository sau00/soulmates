
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var fs   = require('fs');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");




// Создание структуры папок и начальных файлов
// -------------------------------------------------------------------------------
gulp.task('start', startfunc => {
  var folders = [
    'dist/styles/sass',
  ]; // помещаем в массив пути папок
  
  folders.forEach(dir => {
      if(!fs.existsSync(dir)) {
        fs.mkdirSync(dir)
      }  
    }); // для каждого элемента массива создается директива
    
    var sass = source('main.sass'); // объявили переменные для начальных файлов
    
    // записали в начальные файлы текст, иначе файл не создадится
    sass.end('some data');
    
    // выводим начальные файлы в папки
    sass.pipe(gulp.dest('src/styles/sass'));
    
    startfunc(); // async завершение
  })
// -------------------------------------------------------------------------------


// Компиляция sass в css и минификация
// -------------------------------------------------------------------------------
gulp.task('sass', function () {
  return gulp.src('src/styles/sass/**/*.sass') // Выбор файлов
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({ overrideBrowserslist: ['last 2 versions'] }))
    .pipe(gulp.dest('src/styles/')) // Вывод в папку
    .pipe(cleanCSS())
    .pipe(rename({
      suffix: ".min",
      extname: ".css"
    }))
    .pipe(gulp.dest('src/styles/minify/'))
});
// -------------------------------------------------------------------------------





      
// Наблюдение за имзенениями sass и js файлов
// -------------------------------------------------------------------------------
gulp.task('watch', function() {
  gulp.watch('src/styles/sass/**/*.sass', gulp.series('sass'));
  // gulp.watch('dist/scripts/*.js', gulp.series('scripts'));
});
// -------------------------------------------------------------------------------


// Default task
// -------------------------------------------------------------------------------
gulp.task('default', gulp.series('watch'))
// -------------------------------------------------------------------------------
