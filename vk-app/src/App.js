import React, { useEffect } from 'react';
import connect from '@vkontakte/vk-connect';
import "./styles/minify/main.min.css";
import Matching from "./components/matching";
import Actions from "./components/actions";
import Header from "./components/header";
import Stories from "./components/stories";
import OpenDialogue from "./components/openDialogue";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeWindow: <Stories change={this.changestore}/>,
      pollState: true,
      openDialogue: false,
      personData: null
    };
  }

  changeWindow = (activeWindow, headerTitle) => {
    this.setState({
      activeWindow: activeWindow,
      headerTitle: headerTitle
    });
  };

  changestore = count => {
    this.setState({ activeWindow: count });
  };

  openDialog = item => {
    this.setState({
      openDialogue: true,
      personData: item
    });
  };

  render() {
    if (!this.state.pollState) {
      return <Stories pollStatus={this.changePollState} />;
    }

    if (this.state.openDialogue) {
      return <OpenDialogue getPersonData={this.state.personData} />;
    }

    return (
      <div className="view">
        <Header title={this.state.headerTitle} />
        {this.state.activeWindow}
        <Actions
          onChangeWindow={this.changeWindow}
          forChats={this.openDialog}
        />
      </div>
    );
  }
}

export default App;
