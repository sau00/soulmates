import React from 'react';

export default class Chats extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            chats: []
        }
    }

    componentDidMount() {
        const chatsArr = [
            {
                name: 'Alex',
                secondName: 'Tarasiuk',
                message: 'Hello guys!)',
                time: '22:22:03',
                img: 'images/profiles/2.jpg',
                id: '12342342'
            },
            {
                name: 'Amal',
                secondName: 'Usmanov',
                message: 'lorem insput',
                time: '22:22:03',
                img: 'images/profiles/6.jpg',
                id: '2424233'
            },
            {
                name: 'Bashena',
                secondName: 'Besimyannaya',
                message: 'da da ya',
                time: '22:22:03',
                img: 'images/profiles/1.jpg',
                id: '23423425'
            },
            {
                name: 'Kto-to',
                secondName: 'Tam',
                message: 'agagaga YYYUIO',
                time: '22:22:03',
                img: 'images/profiles/8.jpg',
                id: '45644436'
            }
        ];
    
        this.setState({
            chats: chatsArr
        });

    }
    

    render() {
        
        return (
            <div className="chats">
                {this.state.chats.map(item => {
                    return (
                        <div className="message-item" key={item.id} onClick={() => { this.props.openDialog(item) }}>
                            <img src={item.img} alt="text"/>
                            <div className="msg-group">
                                <div className="sender">{item.name} {item.secondName}</div>
                                <div className="msg">{item.message}</div>
                            </div>
                            <div className="time">{item.time}</div>
                        </div>
                    )
                })}
            </div>
        )
    }
}