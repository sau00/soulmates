import React from "react";
import $ from "jquery";
import connect from "@vkontakte/vk-connect";
import { MoonLoader } from "react-spinners";

const apiURL = "https://hackevents.io";

export default class Matching extends React.Component {
  getProfile(person_id) {
    fetch(apiURL + `/api/v1/profile/suggest/${person_id}`)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            person: result
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  coma(date, city) {
    if (date.length > 1 && city.length > 1) {
      return ", ";
    }

    return " ";
  }

  spliter(date) {
    if (date == "") {
      return "";
    }

    var years = date.split(".");
    if (years.length == 3) {
      var nowYear = new Date();
      var answer = nowYear.getFullYear() - years[2];
      return answer;
    } else {
      return "";
    }
  }

  setLike(user_id, person_id) {
    const data = {
      whoLike: {
        id: user_id
      },
      whomLike: {
        id: person_id
      },
      res: "like"
    };

    fetch(apiURL + `/api/v1/profile/like/${user_id}/${person_id}`, {
      // method: "GET", // или 'PUT'
      // body: JSON.stringify(data), // data может быть типа `string` или {object}!
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(response => console.log("Успех:", JSON.stringify(response)))
      .catch(error => console.error("Ошибка:", error));
  }

  sendStory(url, user_id) {
    fetch(apiURL + "/api/v1/stories/send", {
      method: "POST",
      body: JSON.stringify({
        URL: url,
        UserId: user_id.toString()
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => console.log("Успех:", JSON.stringify(response)))
      .catch(error => console.error("Ошибка:", error));
  }

  constructor(props) {
    super(props);

    this.state = {
      user: null,
      //   startPosX: "",
      //   startPosY: "",
      //   touchX: "",
      //   offsetElemX: "",
      //   offsetElemY: "",
      isLoaded: false,
      person: null,
      swipe: false
    };
  }

  componentDidMount() {
    connect
      .sendPromise("VKWebAppGetUserInfo")
      .then(result => {
        console.log(result);
        this.setState({
          user: result
        });
      })
      .then(() => {
        this.getProfile(this.state.user.id);
      })
      // .then(() => {
      //   connect
      //     .sendPromise("VKWebAppGetAuthToken", {
      //       app_id: 7150438,
      //       scope: "stories"
      //     })
      //     .then(res => {
      //       connect
      //         .sendPromise("VKWebAppCallAPIMethod", {
      //           method: "stories.getPhotoUploadServer",
      //           params: {
      //             add_to_news: "1",
      //             v: "5.101",
      //             access_token: res.access_token
      //           }
      //         })
      //         .then(resUrl => {
      //           console.log("sending story to backend");
      //           this.sendStory(resUrl.response.upload_url, this.state.user.id);
      //         })
      //         .catch(console.log);
      //     })
      //     .catch(console.log);
      // })
      .catch(console.log);
  }

  render() {
    if (!this.state.isLoaded) {
      return (
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-md-4 col-md-auto" />
            <MoonLoader
              css={"height: 50px; width: 50px; margin: 0 auto;"}
              loading={this.state.loading}
              color={"#fff"}
              size={50}
            />
          </div>
        </div>
      );
    }
    if (this.state.isLoaded) {
      $(document).ready(() => {
        var like = document.querySelector("#like");
        var dislike = document.querySelector("#dislike");

        const deleteListener = () => {
          like.removeEventListener("touchend", deleteListener);
          dislike.removeEventListener("touchend", deleteListener);
          this.getProfile(this.state.user.id); // получаем данные
          this.setLike(this.state.user.id, this.state.person.Data.Id); // оптправляем данные
        };

        like.addEventListener("touchend", deleteListener);
        dislike.addEventListener("touchend", deleteListener);
      });
    }

    return (
      <div>
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-md-4 col-md-auto">
              <div className="logo text-center">
                <img
                  src="https://sun9-27.userapi.com/c857424/v857424432/8f22d/nbBknvxWj4U.jpg"
                  className="img-fluid"
                />
                soulmates
              </div>
              <div className="profile text-center">
                <div className="profile-bg" />
                <img
                  src={this.state.person.Data.Photo}
                  className="img-fluid rounded"
                />
                <div className="info">
                  <div className="name">
                    {this.state.person.Data.Firstname}
                  </div>
                  <div className="desc">
                    {this.spliter(this.state.person.Data.Age)}
                    {this.coma(
                      this.state.person.Data.Age,
                      this.state.person.Data.City
                    )}
                    {this.state.person.Data.City}
                  </div>
                  <div className="progress">
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{ width: this.state.person.Data.Match + "%" }}
                      aria-valuenow="{this.state.person.Data.Match}"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </div>
              </div>

              <div className="buttons">
                <div className="row">
                  <div className="col-6">
                    <div className="btn btn-lg btn-choise" id="dislike">
                      <i className="fa fa-heart-broken" />
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="btn btn-lg btn-choise" id="like">
                      <i className="fa fa-heart" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer text-center">
          <div class="row">
            <div class="col-4">
              <img
                src="https://sun9-38.userapi.com/c850624/v850624807/1cf16d/WjvGQ0TPJXE.jpg"
                className="img-fluid"
              />
            </div>
            <div class="col-4">
              <img
                src="https://sun9-12.userapi.com/c850732/v850732807/1cba95/MblOPo4KZFc.jpg"
                className="img-fluid"
              />
            </div>
            <div class="col-4">
              <img
                src="https://sun9-42.userapi.com/c858424/v858424807/8b4e3/ly-rjlIHpX8.jpg"
                className="img-fluid"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
