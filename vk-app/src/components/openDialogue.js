import React from 'react';

export default class OpenDialogue extends React.Component {
    render() {
        const personData = this.props.getPersonData;
        return (
            <React.Fragment>
                <div className="chatDialogue">
                    <div className="header">
                        <div className="go-back">
                            <i className="fas fa-arrow-left"></i>
                        </div>
                        <div className="person">
                            <div className="img">
                                <img src={personData.img} alt="img"/>
                            </div>
                            <div className="person-name">
                                {personData.name}  {personData.secondName}
                                <div className="person-status">
                                    online
                                </div>
                            </div>
                            <div className="settings">
                                <i class="fas fa-ellipsis-v"></i>
                            </div>
                        </div>
                        
                    </div>

                    <div className="messages">
                        {personData.message}
                        {personData.time}
                    </div>

                    <form action="">
                        <div className="investments">
                            <i class="fas fa-paperclip"></i>
                        </div>
                        <input type="text"/>
                        <button><i class="fas fa-paper-plane"></i></button>
                    </form>
                </div>
            </React.Fragment>
        )
    }
}