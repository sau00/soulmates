import React from "react";
import $ from "jquery";
import connect from "@vkontakte/vk-connect";
import { MoonLoader } from "react-spinners";
import Matching from "./matching";

const apiURL = "https://hackevents.io";

export default class Stories extends React.Component {
  getProfile(person_id) {
    fetch(apiURL + `/api/v1/profile/suggest/${person_id}`)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            person: result
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  sendStory(url, user_id) {
    fetch(apiURL + "/api/v1/stories/send", {
      method: "POST",
      body: JSON.stringify({
        URL: url,
        UserId: user_id.toString()
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => console.log("Успех:", JSON.stringify(response)))
      .catch(error => console.error("Ошибка:", error));
  }

  randomHex() {
    return (
      "#" +
      (function co(lor) {
        return (lor += [
          0,
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          "a",
          "b",
          "c",
          "d",
          "e",
          "f"
        ][Math.floor(Math.random() * 16)]) && lor.length == 6
          ? lor
          : co(lor);
      })("")
    );
  }

  constructor(props) {
    super(props);

    this.state = {
      user: null
    };
  }

  componentDidMount() {
    // connect
    //   .sendPromise("VKWebAppGetUserInfo")
    //   .then(result => {
    //     console.log(result);
    //     this.setState({
    //       user: result
    //     });
    //   })
    //   .then(() => {
    //     this.getProfile(this.state.user.id);
    //   });
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-md-4 col-md-auto">
              <div className="row">
                <div className="col-sm-8">
                  <br /> <br />
                  <h1>Привет!</h1>
                </div>
                <div className="col-sm-4">
                  {/* <img
                    src="https://sun9-44.userapi.com/c851420/v851420093/1af40b/f_1R5A_aKGQ.jpg"
                    className="img-fluid rounded-circle"
                  /> */}
                </div>
              </div>

              <br />
              <div
                className="row text-center"
                style={{ background: "#7F27D3", borderRadius: "2em" }}
              >
                <div className="col-12">
                  <div className="stories">
                    <br />
                    <h4>
                      Мы проанализировали твою страницу - цвет твоей души:{" "}
                      <span style={{ color: this.randomHex() }}>
                        <strong>{this.randomHex()}</strong>
                      </span>
                    </h4>
                    <br />
                    <div
                      className="btn btn-dark btn-lg"
                      onClick={() => {
                        connect
                          .sendPromise("VKWebAppGetUserInfo")
                          .then(result => {
                            console.log(result);
                            this.setState({
                              user: result
                            });
                          })
                          .then(() => {
                            this.getProfile(this.state.user.id);
                          })
                          .then(() => {
                            connect
                              .sendPromise("VKWebAppGetAuthToken", {
                                app_id: 7150438,
                                scope: "stories"
                              })
                              .then(res => {
                                connect
                                  .sendPromise("VKWebAppCallAPIMethod", {
                                    method: "stories.getPhotoUploadServer",
                                    params: {
                                      add_to_news: "1",
                                      v: "5.101",
                                      access_token: res.access_token
                                    }
                                  })
                                  .then(resUrl => {
                                    console.log("sending story to backend");
                                    this.sendStory(
                                      resUrl.response.upload_url,
                                      this.state.user.id
                                    );
                                    this.props.change(<Matching />);
                                  })
                                  .catch(console.log);
                              })
                              .catch(console.log);
                          })
                          .catch(console.log);
                      }}
                    >
                      Поделиться
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
